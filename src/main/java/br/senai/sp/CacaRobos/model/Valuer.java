package br.senai.sp.CacaRobos.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Valuer {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@NotNull
	private String name;
	@NotNull
	@Column(unique=true)
	private String cpf;
	@NotNull
	@OneToOne(optional=false,fetch=FetchType.EAGER)
	@JoinColumn(name="login_id")
	private Login login;
	@NotNull
	private LocalDate birhadate;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public Login getLogin() {
		return login;
	}
	public void setLogin(Login login) {
		this.login = login;
	}
	public LocalDate getBirhadate() {
		return birhadate;
	}
	public void setBirhadate(LocalDate birhadate) {
		this.birhadate = birhadate;
	}
	@Override
	public String toString() {
		return "Valuer [id=" + id + ", name=" + name + ", cpf=" + cpf + ", login=" + login + ", birhadate=" + birhadate
				+ "]";
	}
	
}
