package br.senai.sp.CacaRobos.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Report {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@NotNull
	private String status;
	@NotNull
	private String description;
	@NotNull
	@JoinColumn(name="user_id")
	@ManyToOne(optional=false,fetch=FetchType.EAGER)
	private User user;
	@NotNull
	@JoinColumn(name="valuer_id")
	@ManyToOne(optional=false,fetch=FetchType.EAGER)
	private Valuer valuer;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Valuer getValuer() {
		return valuer;
	}
	public void setValuer(Valuer valuer) {
		this.valuer = valuer;
	}
	@Override
	public String toString() {
		return "Report [id=" + id + ", status=" + status + ", description=" + description + ", user=" + user
				+ ", valuer=" + valuer + "]";
	}

	
}
