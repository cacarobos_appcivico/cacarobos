package br.senai.sp.CacaRobos.dao;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import br.senai.sp.CacaRobos.model.Valuer;
public class DaoValuer implements GenericDao<Valuer>{
	@PersistenceContext
	private EntityManager manager;
	
	@Transactional
	@Override
	public void create(Valuer t) {
		manager.persist(t);
	}

	@Override
	public Valuer read(Long t) {
		return manager.find(Valuer.class, t);
	}

	@Transactional
	@Override
	public void update(Valuer t) {
		manager.merge(t);
	}

	@Transactional
	@Override
	public void delete(Long t) {
		manager.remove(read(t));
	}

	@Override
	public List<Valuer> listAll() {
		TypedQuery<Valuer> query=manager.createQuery("select v from Valuer v", Valuer.class);
		return query.getResultList();
	}
	
}