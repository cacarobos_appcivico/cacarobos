package br.senai.sp.CacaRobos.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.senai.sp.CacaRobos.model.Report;

@Repository
public class DaoReport implements GenericDao<Report>{
	@PersistenceContext
	private EntityManager manager;

	@Transactional
	@Override
	public void create(Report t) {
		manager.persist(t);
	}

	@Override
	public Report read(Long t) {
		return manager.find(Report.class, t);
	}

	@Transactional
	@Override
	public void update(Report t) {
		manager.merge(t);
	}

	@Transactional
	@Override
	public void delete(Long t) {
		manager.remove(read(t));
	}

	@Override
	public List<Report> listAll() {
		TypedQuery<Report> query=manager.createQuery("select r from Report r",Report.class);
		return query.getResultList();
	}
	
}
