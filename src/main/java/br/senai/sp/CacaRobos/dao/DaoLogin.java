package br.senai.sp.CacaRobos.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.senai.sp.CacaRobos.model.Login;

@Repository
public class DaoLogin implements GenericDao<Login>{
	
	@PersistenceContext
	private EntityManager manager;

	@Transactional
	@Override
	public void create(Login t) {
		manager.persist(t);
	}

	@Override
	public Login read(Long t) {
		return manager.find(Login.class, t);
	}

	@Transactional
	@Override
	public void update(Login t) {
		manager.merge(t);
	}

	@Transactional
	@Override
	public void delete(Long t) {
		manager.remove(read(t));
	}

	@Override
	public List<Login> listAll() {
		TypedQuery<Login> query=manager.createQuery("select l from Login l",Login.class);
		return query.getResultList();
	}
	
	
}