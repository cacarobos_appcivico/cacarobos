package br.senai.sp.CacaRobos.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.senai.sp.CacaRobos.model.Comment;

@Repository
public class DaoComment implements GenericDao<Comment>{

	@PersistenceContext
	private EntityManager manager;
	
	@Transactional
	@Override
	public void create(Comment t) {
		manager.persist(t);
	}

	@Override
	public Comment read(Long t) {
		return manager.find(Comment.class, t);
	}

	@Transactional
	@Override
	public void update(Comment t) {
		manager.merge(t);
	}

	@Transactional
	@Override
	public void delete(Long t) {
		manager.remove(read(t));
	}

	@Override
	public List<Comment> listAll() {
		TypedQuery<Comment> query=manager.createQuery("select c from Comment c",Comment.class);
		return query.getResultList();
	}

}
