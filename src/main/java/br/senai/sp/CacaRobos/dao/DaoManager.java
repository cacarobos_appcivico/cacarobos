package br.senai.sp.CacaRobos.dao;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import br.senai.sp.CacaRobos.model.Manager;
@Repository
public class DaoManager implements GenericDao<Manager>{

	@PersistenceContext
	private EntityManager manager;
	
	@Transactional
	@Override
	public void create(Manager t) {
		manager.persist(t);
	}
	
	@Override
	public Manager read(Long t) {
		return manager.find(Manager.class, t);
	}

	@Transactional
	@Override
	public void update(Manager t) {
		manager.merge(t);
	}

	@Transactional
	@Override
	public void delete(Long t) {
		manager.remove(read(t));
	}

	@Override
	public List<Manager> listAll() {
		TypedQuery<Manager> query=manager.createQuery("select m from Manager m", Manager.class);
		return query.getResultList();
	}

}
