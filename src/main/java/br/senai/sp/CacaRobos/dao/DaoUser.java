package br.senai.sp.CacaRobos.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import br.senai.sp.CacaRobos.model.User;

@Repository
public class DaoUser implements GenericDao<User>{
	@PersistenceContext
	private EntityManager manager;
	
	@Transactional
	@Override
	public void create(User t) {
		manager.persist(t);
	}

	@Override
	public User read(Long t) {
		return manager.find(User.class, t);
	}

	@Transactional
	@Override
	public void update(User t) {
		manager.merge(t);
	}

	@Transactional
	@Override
	public void delete(Long t) {
		manager.remove(read(t));
	}

	@Override
	public List<User> listAll() {
		TypedQuery<User> query=manager.createQuery("select u from User u", User.class);
		return query.getResultList();
	}

}
